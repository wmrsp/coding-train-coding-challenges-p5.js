var canvasDivId = 'clock';
var canvasWidth = document.getElementById(canvasDivId).offsetWidth;
var canvasHeight = document.getElementById(canvasDivId).offsetHeight;
var handLength = 50;
var dToTop;
var dToSide;
var arcHeight;
var arcWidth;
var strokeThickness;
var slider;

function setup() {
    angleMode(DEGREES);
    var canvas = createCanvas(canvasWidth, canvasHeight);
    canvas.parent(canvasDivId);
    dToSide = canvasWidth / 2;
    dToTop = canvasHeight / 2;
    slider = createSlider(10, 1000, 400, 1);
}

function draw() {
    background(0); // make background black
    translate(dToSide, dToTop); // move to the center of the canvas
    rotate(-90);
    strokeThickness = (canvasWidth >= canvasHeight?canvasHeight * 0.02:canvasWidth * 0.02);
    strokeWeight(strokeThickness); // 2% of canvas width
    noFill();

    document.getElementById('defaultCanvas0').style.width = slider.value() + "px";

    let hr = hour();
    let mn = minute();
    let sc = second();
    let secondAngle = map(sc, 0, 60, 0, 360);
    let minuteAngle = map(mn, 0, 60, 0, 360);
    let hourAngle = map(hr % 12, 0, 12, 0, 360);

    // extra arc
    arcHeight = canvasHeight * 0.80;
    arcWidth = canvasWidth * 0.80;
    stroke(255, 255, 100);
    arc(0, 0, arcHeight, arcWidth, 0, 360);

    // hours arc
    arcHeight = canvasHeight * 0.65;
    arcWidth = canvasWidth * 0.65;
    handLength = ellipseRadius(arcHeight, arcWidth, hourAngle);
    stroke(150, 255, 100);
    arc(0, 0, arcHeight, arcWidth, 0, hourAngle);
    // hours hand
    push();
    rotate(hourAngle);
    line(0, 0, handLength, 0);
    pop();

    // minutes arc
    arcHeight = canvasHeight * 0.70;
    arcWidth = canvasWidth * 0.70;
    handLength = ellipseRadius(arcHeight, arcWidth, minuteAngle);
    stroke(150, 100, 255);
    arc(0, 0, arcHeight, arcWidth, 0, minuteAngle);
    // minutes hand
    push();
    rotate(minuteAngle);
    line(0, 0, handLength, 0);
    pop();

    // seconds arc
    arcHeight = canvasHeight * 0.75;
    arcWidth = canvasWidth * 0.75;
    handLength = ellipseRadius(arcHeight, arcWidth, secondAngle);
    stroke(255, 100, 150);
    arc(0, 0, arcHeight, arcWidth, 0, secondAngle);
    // seconds hand
    push();
    rotate(secondAngle);
    line(0, 0, handLength, 0);
    pop();

    stroke(255);
    point(0, 0);
}

function ellipseRadius(a, b, angle) {
    return ((a * b) / sqrt( (Math.pow(a, 2) * Math.pow(sin(angle), 2)) + (Math.pow(b, 2) * Math.pow(cos(angle), 2)) )) / 2;
}
