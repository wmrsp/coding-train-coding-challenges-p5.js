function setup() {
    createCanvas(400, 400);
    angleMode(DEGREES);
}

function draw() {
    background(0);
    translate(200, 200);
    rotate(-90);
    strokeWeight(8);
    noFill();

    let hr = hour();
    let mn = minute();
    let sc = second();
    let secondAngle = map(sc, 0, 60, 0, 360);
    let minuteAngle = map(mn, 0, 60, 0, 360);
    let hourAngle = map(hr % 12, 0, 12, 0, 360);

    // extra arc
    stroke(255, 255, 100);
    arc(0, 0, 320, 320, 0, 360);

    // seconds arc
    stroke(255, 100, 150);
    arc(0, 0, 300, 300, 0, secondAngle);
    // seconds hand
    push();
    rotate(secondAngle);
    line(0, 0, 120, 0);
    pop();

    // minutes arc
    stroke(150, 100, 255);
    arc(0, 0, 280, 280, 0, minuteAngle);
    // minutes hand
    push();
    rotate(minuteAngle);
    line(0, 0, 100, 0);
    pop();

    // hours arc
    stroke(150, 255, 100);
    arc(0, 0, 260, 260, 0, hourAngle);
    // hours hand
    push();
    rotate(hourAngle);
    line(0, 0, 80, 0);
    pop();

    // point in the middle
    stroke(255);
    point(0, 0);
}
