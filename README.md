# [The Coding Train](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw)

## Don't forget to run `npm install` before attempting to run the sketches.

* #003 - The Snake Game - https://www.youtube.com/watch?v=AaGK-fj-BAM
* #014 - Fractal Trees Recursive - https://www.youtube.com/watch?v=0jjeOYMjmDU
* #015 - Fractal Trees Object oriented - https://www.youtube.com/watch?v=fcdNSZ9IzJM
* #074 - Clock - https://www.youtube.com/watch?v=E4RyStef-gY
* #117 - Seven Segment Display - https://www.youtube.com/watch?v=b8RwO1k9DXc
* #149 - Tic Tac Toe - https://www.youtube.com/watch?v=GTWrWM1UsnA