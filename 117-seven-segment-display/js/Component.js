function Component(x, y, width)
{
    this.x = x;
    this.y = y;
    this.width = width * .65; // * .65 being arbitrary in order to allow for the decimal point to fit and then neatly fit another component next to it.
    this.segments = {};

    this.draw = function(hex_value, decimal_point = false)
    {
        let stroke = this.width / 4;
        let margin = 0.02 * this.width;

        let hor_x = this.x + (stroke / 2) + margin;
        let hor_top_y = this.y;
        let hor_middle_y = this.y + (this.width - (stroke / 2)) + margin;
        let hor_bottom_y = hor_middle_y + (this.width - (stroke / 2)) + margin;
        let hor_width = this.width - (2 * margin) - (this.width / 7.5); // (this.width / 7.5) being arbitrary in order to make to component look better (more square).

        let ver_left_x = this.x + stroke;
        let ver_right_x = ver_left_x + this.width - (this.width / 7.5); // (this.width / 7.5) being arbitrary in order to make to component look better (more square).
        let ver_top_y = this.y + (stroke / 2) + margin;
        let ver_bottom_y = this.y + this.width + (2 * margin);
        let ver_width = this.width - (stroke / 2) - margin;

        for (let i = 0; i < 8; i++) {
            // https://en.wikipedia.org/wiki/Seven-segment_display
            // Segment order: A, B, C, D, E, F, G, DP
            let on_off = (hex_value >> i) & 1;
            switch (i) {
                case 0:
                    this.segments.G = new Segment(hor_x, hor_middle_y, hor_width, stroke, 0);
                    if (on_off) {
                        this.segments.G.switchOn();
                    }
                    else {
                        this.segments.G.switchOff();
                    }
                    break;
                case 1:
                    this.segments.F = new Segment(ver_left_x, ver_top_y, ver_width, stroke, (PI/2));
                    if (on_off) {
                        this.segments.F.switchOn();
                    }
                    else {
                        this.segments.F.switchOff();
                    }
                    break;
                case 2:
                    this.segments.E = new Segment(ver_left_x, ver_bottom_y, ver_width, stroke, (PI / 2));
                    if (on_off) {
                        this.segments.E.switchOn();
                    }
                    else {
                        this.segments.E.switchOff();
                    }
                    break;
                case 3:
                    this.segments.D = new Segment(hor_x, hor_bottom_y, hor_width, stroke, 0);
                    if (on_off) {
                        this.segments.D.switchOn();
                    }
                    else {
                        this.segments.D.switchOff();
                    }
                    break;
                case 4:
                    this.segments.C = new Segment(ver_right_x, ver_bottom_y, ver_width, stroke, (PI/2));
                    if (on_off) {
                        this.segments.C.switchOn();
                    }
                    else {
                        this.segments.C.switchOff();
                    }
                    break;
                case 5:
                    this.segments.B = new Segment(ver_right_x, ver_top_y, ver_width, stroke, (PI/2));
                    if (on_off) {
                        this.segments.B.switchOn();
                    }
                    else {
                        this.segments.B.switchOff();
                    }
                    break;
                case 6:
                    this.segments.A = new Segment(hor_x, hor_top_y, hor_width, stroke, 0);
                    if (on_off) {
                        this.segments.A.switchOn();
                    }
                    else {
                        this.segments.A.switchOff();
                    }
                    break;
                case 7:
                    this.segments.DP = new DecimalPoint((ver_right_x + (stroke / 2)), (ver_bottom_y + ver_width + margin), stroke, stroke);
                    // if (on_off) {
                    if (decimal_point) {
                        this.segments.DP.switchOn();
                    }
                    else {
                        this.segments.DP.switchOff();
                    }
                    break;
            }
        }
    };
}
