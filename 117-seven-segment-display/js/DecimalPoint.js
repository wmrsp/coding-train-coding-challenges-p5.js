function DecimalPoint(x, y, width, height)
{
    this.x = x;
    this.y = y;
    this.width = width;
    this.onColor = color('rgba(255,0,0,1)');
    this.offColor = color('rgba(255,0,0,.1)');

    this.switchOn = function()
    {
        this.draw(this.onColor);
    };

    this.switchOff = function()
    {
        this.draw(this.offColor);
    };

    this.draw = function(draw_color)
    {
        noStroke();
        fill(draw_color);
        // rect(this.x, this.y, this.width, height);
        ellipse(this.x, this.y, this.width);
    };
}