function Segment(x, y, width, height, rotation = 0)
{
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.onColor = color('rgba(255,0,0,1)');
    this.offColor = color('rgba(255,0,0,.1)');
    this.rotation = rotation;

    this.switchOn = function()
    {
        this.draw(this.onColor);
    };

    this.switchOff = function()
    {
      this.draw(this.offColor);
    };

    this.draw = function(draw_color)
    {
        push();
        noStroke();
        fill(draw_color);
        translate(x, y);
        rotate(this.rotation);
        let new_x = 0;
        let new_y = 0;
        let triangle_width    = this.height * 0.5;
        let triangle_top_y    = new_y;
        let triangle_middle_y = new_y + (this.height * 0.5);
        let triangle_bottom_y = new_y + this.height;
        let rect_x     = (new_x + triangle_width);
        let rect_width = (this.width - (2 * triangle_width));
        triangle(
            new_x                   , triangle_middle_y,
            (new_x + triangle_width), triangle_top_y,
            (new_x + triangle_width), triangle_bottom_y
        );
        rect(
            rect_x    , new_y,
            rect_width, this.height
        );
        triangle(
            (rect_x + rect_width)                 , triangle_top_y,
            (rect_x + rect_width + triangle_width), triangle_middle_y,
            (rect_x + rect_width)                 , triangle_bottom_y
        );
        pop();
    };
}
