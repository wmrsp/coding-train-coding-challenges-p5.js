function Clock(x, y, width, time)
{
    this.x = x;
    this.y = y;
    this.component_width = width / 8;
    this.components = {};
    this.time = {};

    this.getHexNumber = function(num)
    {
        let numbers = {
            '0': 0x7E,
            '1': 0x30,
            '2': 0x6D,
            '3': 0x79,
            '4': 0x33,
            '5': 0x5B,
            '6': 0x5F,
            '7': 0x70,
            '8': 0x7F,
            '9': 0x7B,
            'a': 0x77,
            'b': 0x1F,
            'c': 0x4E,
            'd': 0x3D,
            'e': 0x4F,
            'f': 0x47
        };
        return numbers[num];
    };

    let time_arr = time.split(':');
    for (let i = 0; i < time_arr.length; i++) {
        switch (i) {
            case 0:
                this.time.hour = time_arr[i].split("");
                break;
            case 1:
                this.time.minute = time_arr[i].split("");
                break;
            case 2:
                this.time.second = time_arr[i].split("");
                break;
        }
    }

    for (let i = 0; i < 6; i++) {
        switch (i) {
            case 0:
                this.components.hour_1 = new Component((this.x + (i * this.component_width)), this.y, this.component_width);
                this.components.hour_1.draw(this.getHexNumber(this.time.hour[0]));
                break;
            case 1:
                this.components.hour_2 = new Component((this.x + (i * this.component_width)), this.y, this.component_width);
                this.components.hour_2.draw(this.getHexNumber(this.time.hour[1]),true);
                break;
            case 2:
                this.components.minute_1 = new Component((this.x + (i * this.component_width)), this.y, this.component_width);
                this.components.minute_1.draw(this.getHexNumber(this.time.minute[0]));
                break;
            case 3:
                this.components.minute_2 = new Component((this.x + (i * this.component_width)), this.y, this.component_width);
                this.components.minute_2.draw(this.getHexNumber(this.time.minute[1]),true);
                break;
            case 4:
                this.components.second_1 = new Component((this.x + (i * this.component_width)), this.y, this.component_width);
                this.components.second_1.draw(this.getHexNumber(this.time.second[0]));
                break;
            case 5:
                this.components.second_2 = new Component((this.x + (i * this.component_width)), this.y, this.component_width);
                this.components.second_2.draw(this.getHexNumber(this.time.second[1]));
                break;
        }
    }
}
