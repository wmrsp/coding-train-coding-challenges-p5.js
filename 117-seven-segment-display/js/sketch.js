function setup() {
    createCanvas(475,125);
}

function draw() {
    background(200);
    let h = hour() > 9 ? hour() : '0' + hour();
    let m = minute() > 9 ? minute() : '0' + minute();
    let s = second() > 9 ? second() : '0' + second();
    let time = h + ':' + m + ':' + s;
    new Clock(10, 10, 600, time);
}