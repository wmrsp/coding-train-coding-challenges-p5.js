class Game {

    constructor (x, y, width, height, rows, cols, numInRowToWin, autoRestart) {
        // set settings
        this.squares = [];
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rows = rows;
        this.cols = cols;
        this.numInRowToWin = numInRowToWin;
        this.autoRestart = autoRestart;
        this.dividers = [];
        this.currentPlayer = undefined;
        this.winner = undefined;
        // create squares
        let squareWidth = this.width / this.cols;
        let squareHeight = this.height / this.rows;
        for (let row = 0;row < this.rows;row++) {
            this.squares.push([]);
            for (let col = 0;col < this.cols;col++) {
                let squareX = this.x + (squareWidth * col);
                let squareY = this.y + (squareHeight * row);
                this.squares[row].push(new Square(squareX, squareY, squareWidth, squareHeight, row.toString() + '.' + col.toString()));
                // vertical lines between squares
                if (col > 0 && col < this.cols && row < 1) {
                    this.dividers.push({
                        x1: squareX,
                        y1: squareY,
                        x2: squareX,
                        y2: squareY + this.height
                    });
                }
                // horizontal lines between squares
                if (row > 0 && row < this.rows && col < 1) {
                    this.dividers.push({
                        x1: squareX,
                        y1: squareY,
                        x2: squareX + this.width,
                        y2: squareY
                    });
                }
            }
        }
        // let the first player begin
        this.currentPlayer = 1;
    }

    update (currentPlayer, nextPlayer, Square, callback = undefined) {
        // check if Square is available
        if (Square.available) {
            // take the Square
            Square.available = false;
            Square.player = currentPlayer;
            // check if this led to a winner
            let squaresAvailable = 0;
            for (let row = 0;row < this.squares.length;row++) {
                for (let col = 0;col < this.squares[row].length;col++) {
                    // check if this square is still available
                    if (this.squares[row][col].available) {
                        squaresAvailable++;
                    }
                    let squaresToCheck;
                    // horizontal lines
                    if (this.squares[row] && this.squares[row][col + (this.numInRowToWin - 1)]) {
                        squaresToCheck = [];
                        for (let squareToCheck = col;squareToCheck < (col + this.numInRowToWin);squareToCheck++) {
                            squaresToCheck.push(this.squares[row][squareToCheck]);
                        }
                        // https://stackoverflow.com/questions/14832603/check-if-all-values-of-array-are-equal
                        if (squaresToCheck.every((val, i, arr) => val.player === arr[0].player && !val.available)) {
                            this.winner = squaresToCheck;
                        }
                    }
                    // vertical lines
                    if (this.squares[row + (this.numInRowToWin - 1)] && this.squares[row + (this.numInRowToWin - 1)][col]) {
                        squaresToCheck = [];
                        for (let squareToCheck = row;squareToCheck < (row + this.numInRowToWin);squareToCheck++) {
                            squaresToCheck.push(this.squares[squareToCheck][col]);
                        }
                        // https://stackoverflow.com/questions/14832603/check-if-all-values-of-array-are-equal
                        if (squaresToCheck.every((val, i, arr) => val.player === arr[0].player && !val.available)) {
                            this.winner = squaresToCheck;
                        }
                    }
                    // diagonal lines
                    if (this.squares[row + (this.numInRowToWin - 1)] && this.squares[row + (this.numInRowToWin - 1)][col + (this.numInRowToWin - 1)]) {
                        let rowToCheck = row;
                        squaresToCheck = [];
                        for (let squareToCheck = col;squareToCheck < (col + this.numInRowToWin);squareToCheck++) {
                            squaresToCheck.push(this.squares[rowToCheck][squareToCheck]);
                            rowToCheck++;
                        }
                        // https://stackoverflow.com/questions/14832603/check-if-all-values-of-array-are-equal
                        if (squaresToCheck.every((val, i, arr) => val.player === arr[0].player && !val.available)) {
                            this.winner = squaresToCheck;
                        }
                    }
                    if (this.squares[row - (this.numInRowToWin - 1)] && this.squares[row - (this.numInRowToWin - 1)][col + (this.numInRowToWin - 1)]) {
                        let rowToCheck = row;
                        squaresToCheck = [];
                        for (let squareToCheck = col;squareToCheck < (col + this.numInRowToWin);squareToCheck++) {
                            squaresToCheck.push(this.squares[rowToCheck][squareToCheck]);
                            rowToCheck--;
                        }
                        // https://stackoverflow.com/questions/14832603/check-if-all-values-of-array-are-equal
                        if (squaresToCheck.every((val, i, arr) => val.player === arr[0].player && !val.available)) {
                            this.winner = squaresToCheck;
                        }
                    }
                }
            }
            if (squaresAvailable === 0 && !this.winner) {
                this.winner = 'tie';
            }
            // pass the turn to the next player
            this.currentPlayer = nextPlayer;
        }
        // perform callback if applicable
        if (callback) { callback(); }
    }
}