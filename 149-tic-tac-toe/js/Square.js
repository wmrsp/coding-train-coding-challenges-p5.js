class Square {

    constructor (x, y, width, height, id) {
        // set settings
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.middle = undefined;
        this.available = undefined;
        this.player = undefined;
        // calculate middle
        this.middle = {
            x: this.x + (this.width / 2),
            y: this.y + (this.height / 2)
        };
        // set availability
        this.available = true;
    }
}